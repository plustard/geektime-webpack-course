# geektime-webpack-course
《玩转webpack》极客时间课程源码和课件

## Pull Request
  向开源项目贡献代码
  Pull 
  git pull origin master --rebase
  发现我们 fork 的项目, 必须是开源  license
  反馈给原项目, 给开源项目提一个pr

  xiaoyuan

## 公众号
扫码关注公众号，定期推送 webpack 及其它前端技术文章，前期推送频率每周一篇 webpack 文章。

![](https://qpic.url.cn/feeds_pic/Q3auHgzwzM6YgWVL5uiajG7OGGNPWyibs46MZAqPk5LOfkodFLqJfJyA/)
